import java.util.Scanner;

public class OX2 {

	static char[][] board = { { '-', '-', '-' }, { '-', '-', '-' }, { '-', '-', '-' } };
	static char player = 'X';
	static int row = 0, col = 0;
	static Scanner kb = new Scanner(System.in);

	public static void main(String[] args) {
		showWelcome();
		while (true) {
			showBoard();
			showTurn();
			showInput();
			if (checkWin()) {
				break;
			} else if (checkDraw()) {
				break;
			}
			switchingSite();
		}
		showBoard();
		showWin();
		showBye();

	}

	public static void showWelcome() {
		System.out.println("Welcome to OX Game");
	}

	public static void showBye() {
		System.out.println("Bye Bye . . .");
	}

	public static void showBoard() {
		System.out.println("  1 2 3");
		for (int i = 0; i < 3; i++) {
			System.out.print(i + 1);
			for (int j = 0; j < 3; j++) {
				System.out.print(" " + board[i][j]);
			}
			System.out.println();
		}
	}

	public static void showTurn() {
		System.out.println("Turn " + player);
	}

	public static void switchingSite() {
		if (player == 'X') {
			player = 'O';
		} else if (player == 'O') {
			player = 'X';
		}
	}

	public static void putSite(int row, int col) {
		if (row < 1 || row > 3 || col < 1 || col > 3) {
			System.out.println("This col/row is not available.");
			return;
		}
		if (board[row - 1][col - 1] == 'X' || board[row - 1][col - 1] == 'O') {
			System.out.println("This col/row is already occupied.");
			return;
		}
		board[row - 1][col - 1] = player;
	}

	public static void showInput() {
		System.out.print("Please input row / col : ");
		row = kb.nextInt();
		col = kb.nextInt();
		putSite(row, col);
	}

	public static void showWin() {
		if (checkWin()) {
			System.out.println("Player " + player + " Win...!");
		} else if (checkDraw()) {
			System.out.println("Draw...!");
		} else {
			switchingSite();
		}
	}

	public static boolean checkWin() {
		if (checkHorizon(row - 1) == true) {
			return true;
		} else if (checkVerticle(col - 1) == true) {
			return true;
		} else if (checkX1()) {
			return true;
		} else if (checkX2()) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean checkHorizon(int row) {
		if (board[row][0] == board[row][1] && board[row][0] == board[row][2] && board[row][0] != '-') {
			return true;
		} else {
			return false;
		}
	}

	public static boolean checkVerticle(int col) {
		if (board[0][col] == board[1][col] && board[0][col] == board[2][col] && board[0][col] != '-') {
			return true;
		} else {
			return false;
		}
	}

	public static boolean checkX1() {
		if (board[1][1] != '-') {
			if (board[0][0] == board[1][1] && board[1][1] == board[2][2]) {
				return true;
			}
		}
		return false;
	}

	public static boolean checkDraw() {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (board[i][j] == '-') {
					return false;
				}
			}
		}
		return true;

	}

	public static boolean checkX2() {
		if (board[1][1] != '-') {
			if (board[0][2] == board[1][1] && board[1][1] == board[2][0]) {
				return true;
			}
		}
		return false;
	}

}
